package com.example.demo.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.example.demo.service.article.ArticleService;

public class Article {

	@NotNull
	private int id;
	@NotBlank
	private String title;
	@NotBlank
	@Size(min=5,max=50)
	private String description;
	@NotBlank
	private String author;
	private String created_date;
	private String thumbnail;
	private Category category;
	
	public Article() {
		
	}
	public Article(int id, String title, String description, String author, String created_date, String thumnail,Category category) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.author = author;
		this.created_date = created_date;
		this.thumbnail = thumnail;
		this.category = category;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getCreated_date() {
		return created_date;
	}
	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}
	public String getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(String thumnail) {
		this.thumbnail = thumnail;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	@Override
	public String toString() {
		return "Article [id=" + id + ", title=" + title + ", description=" + description + ", author=" + author
				+ ", created_date=" + created_date + ", thumnail=" + thumbnail + ", category=" + category + "]";
	}
	

}
