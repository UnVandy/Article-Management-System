package com.example.demo.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
@PropertySource("classpath:ams.properties")
public class FileUploadController {

	@Value("${file.upload.server.path}")
	private String serverPath;
	
	@GetMapping("/upload")
	public String upload() {
		return "upload";
	}
	
	@PostMapping("/upload")
	public String saveFile(@RequestParam("file") MultipartFile file) {
		if(!file.isEmpty()) {
			try {
				Files.copy(file.getInputStream(), Paths.get(serverPath, file.getOriginalFilename()));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return"redirect:/upload";
	}
}
