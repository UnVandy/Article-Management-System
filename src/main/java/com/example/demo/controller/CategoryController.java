package com.example.demo.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import com.example.demo.model.Category;
import com.example.demo.service.category.CategoryService;

@Controller
public class CategoryController {
	
	@Autowired
	private CategoryService categoryService;
	
//	Show
	@GetMapping("showcategory")
	public String home(Model m) {
		List<Category> categories=categoryService.findAll();
		m.addAttribute("category",categories);
		//System.out.println(categoryService.findAll());
		return "category";
	}
	
	@GetMapping("category")
	public String category(ModelMap m) {
		m.addAttribute("category",new Category());
		//m.addAttribute("category",categoryService.findAll());
		return "category";
	}
	
	@PostMapping("category")
	public String save(@ModelAttribute Category category) {
		categoryService.add(category);
		return "redirect:/category";
	}
}
