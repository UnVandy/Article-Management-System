package com.example.demo.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.model.Article;
import com.example.demo.service.article.ArticleService;
import com.example.demo.service.category.CategoryService;

@Controller
@PropertySource("classpath:ams.properties")
public class ArticleController {
	
	@Value("${file.upload.server.path}")
	private String serverPath;
	
	@Autowired
	private ArticleService articleService;
	@Autowired
	private CategoryService categoryService;
	
//	Show
	@GetMapping("/article")
	public String home(Model m) {
		List<Article> articles=articleService.findAll();
		m.addAttribute("articles",articles);
		return "article";
	}
	
//	Add
	@GetMapping("/add")
	public String add(ModelMap m) {
		m.addAttribute("article",new Article());
		m.addAttribute("categories",categoryService.findAll());
		m.addAttribute("formAdd",true);
		return "add";
	}
	
	@PostMapping("/add")
	public String saveArticle(@Valid @ModelAttribute Article article,BindingResult result,ModelMap m,@RequestParam("file") MultipartFile file) {
		Date d=new Date();
		SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
//		SimpleDateFormat sdfs=new SimpleDateFormat("dd/MM/yyyy/hh:mm:ss");
		if(result.hasErrors()) {
			m.addAttribute("article",article);
			m.addAttribute("categories",categoryService.findAll());
			m.addAttribute("formAdd",true);
			System.out.println("Error");
			return "add";
		}
		
//		Upload image
		if(!file.isEmpty()) {
			try {
				Files.copy(file.getInputStream(), Paths.get(serverPath, file.getOriginalFilename()));
				article.setThumbnail("/image/"+file.getOriginalFilename());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		article.setCategory(categoryService.findOne(article.getCategory().getId()));
		article.setCreated_date(sdf.format(d));
		articleService.add(article);
		return "redirect:/add";
	}
	
	
//	Delete
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id") int id) {
		articleService.delete(id);
		return "redirect:/article";
	}
	
//	Update
	@GetMapping("/update/{id}")
	public String update(@PathVariable int id,ModelMap m) {
		m.addAttribute("article",articleService.findOne(id));
		m.addAttribute("categories",categoryService.findAll());
		m.addAttribute("formAdd",false);
		return "add";
	}
	
	@PostMapping("/update")
	public String saveUpdate(@RequestParam("file") MultipartFile file,@ModelAttribute Article article) {
		if(!file.isEmpty()) {
				try {
					Files.copy(file.getInputStream(), Paths.get(serverPath, file.getOriginalFilename()));
				} catch (IOException e) {
					
					e.printStackTrace();
				}
			article.setThumbnail("/image/"+file.getOriginalFilename());
		}else {
			article.setThumbnail(article.getThumbnail());
		}
		
		
		article.setCategory(categoryService.findOne(article.getCategory().getId()));
		articleService.update(article);
		System.out.println(article.toString());
		return "redirect:/article";
	}
	
}
