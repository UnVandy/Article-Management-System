package com.example.demo.repository.article;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.example.demo.model.Category;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Article;
import com.github.javafaker.Faker;

//@Repository
public class ArticleRepositoryImp implements ArticleRepository {
	private List<Article> articles=new ArrayList<Article>();

	public ArticleRepositoryImp() {
		Faker f=new Faker();
		Date d=new Date();
		SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
		for(int i=1;i<11;i++) {
			articles.add(new Article(i, f.book().title(), f.book().title(), f.artist().name(),sdf.format(d),"http://localhost:8080/image/dy.jpeg",new Category(4,"Database")));
		}
	}
	
	@Override
	public void add(Article article) {
		articles.add(article);
		
	}

	@Override
	public Article findOne(int id) {
		for(Article article:articles) {
			if(article.getId()==id)
				return article;
		}
		return null;
	}

	

	@Override
	public void delete(int id) {
		for(Article article:articles) {
			if(article.getId()==id) {
				articles.remove(article);
				return;
			}
		}
		
	}

	@Override
	public void update(Article article) {
		for(int i=0;i<articles.size();i++) {
			if(articles.get(i).getId()==article.getId()) {
				articles.get(i).setTitle(article.getTitle());
				articles.get(i).setCategory(article.getCategory());
				articles.get(i).setDescription(article.getDescription());
				articles.get(i).setAuthor(article.getAuthor());
				return;
			}
		}
	}

	@Override
	public List<Article> findAll() {
		return articles;
	}

}
