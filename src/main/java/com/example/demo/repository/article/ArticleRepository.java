package com.example.demo.repository.article;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Article;

@Repository
public interface ArticleRepository {

	@Insert("INSERT INTO tb_article(title,description,author,created_date,thumbnail,category_id) VALUES(#{title},#{description},#{author},#{created_date},#{thumbnail},#{category.id})")
	public void add(Article article);
	
	@Select("SELECT a.id, a.title,a.description,a.author,a.created_date,a.thumbnail,a.category_id,c.name as category_name FROM tb_article a INNER JOIN tb_categories c ON a.category_id=c.id WHERE a.id=#{id}")
	@Results({
		@Result(property="id",column="id"),
		@Result(property="title",column="title"),
		@Result(property="description",column="description"),
		@Result(property="author",column="author"),
		@Result(property="created_date",column="created_date"),
		@Result(property="thumbnail",column="thumbnail"),
		@Result(property="category.id",column="category_id"),
		@Result(property="category.name",column="category_name")
		
	})
	public Article findOne(int id);
	
	@Select("SELECT a.id, a.title,a.description,a.author,a.created_date,a.thumbnail,a.category_id,c.name as category_name FROM tb_article a INNER JOIN tb_categories c ON a.category_id=c.id ORDER BY a.id ASC")
	@Results({
		@Result(property="created_date",column="created_date"),
		@Result(property="category.id",column="category_id"),
		@Result(property="category.name",column="category_name")
		
	})
	public List<Article> findAll();
	
	@Delete("DELETE FROM tb_article WHERE id =#{id}")
	public void delete(int id);
	
	@Update("UPDATE tb_article SET title=#{title},description=#{description},author=#{author},thumbnail=#{thumbnail},category_id=#{category.id} WHERE id= #{id} ")
	public void update(Article article);
}
