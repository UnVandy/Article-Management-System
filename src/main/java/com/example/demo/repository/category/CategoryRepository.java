package com.example.demo.repository.category;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Article;
import com.example.demo.model.Category;

@Repository
public interface CategoryRepository {
	
	@Select("SELECT id,name FROM tb_categories ORDER BY ID ASC")
	public List<Category> findAll();
	@Select("SELECT id,name FROM tb_categories WHERE id = #{id}")
	public Category findOne(int id);
	
	@Insert("INSERT INTO tb_categories(name) VALUES(#{name})")
	public void add(Category category);
}
