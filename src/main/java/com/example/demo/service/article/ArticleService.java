package com.example.demo.service.article;

import java.util.List;

import com.example.demo.model.Article;

public interface ArticleService {
	void add(Article article);
	Article findOne(int id);
	List<Article>findAll();
	void delete(int id);
	void update(Article article);
}
