package com.example.demo.service.article;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Article;
import com.example.demo.repository.article.ArticleRepository;

@Service
public class ArticleServiceImp implements ArticleService{

	@Autowired
	private ArticleRepository articleRepo;
	@Override
	public void add(Article article) {
		articleRepo.add(article);
	}

	@Override
	public Article findOne(int id) {
		return articleRepo.findOne(id);
	}

	@Override
	public List<Article> findAll() {
		return articleRepo.findAll();
	}
	
	@Override
	public void delete(int id) {
		articleRepo.delete(id);
	}

	@Override
	public void update(Article article) {
		articleRepo.update(article);
		
	}
}
